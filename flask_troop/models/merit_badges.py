"""
merit_badges.py

models a merit badge requirement and whether a scout still needs it

"""
from sqlalchemy import (
    Column,
    String,
    Integer,
    Boolean,
    ForeignKey,
    ForeignKeyConstraint,
)
from sqlalchemy.orm import relationship
from flask_troop.models.base import Base


class MBRequirement(Base):
    __tablename__ = "mb_requirement"
    badge = Column(String, primary_key=True)
    code = Column(String, primary_key=True)
    needs = relationship("MBReqNeed", back_populates="requirement")

    def __repr__(self):
        return f"<MB Requirement: {self.badge_name}, {self.code}>"


class MBReqNeed(Base):
    __tablename__ = "mb_req_needs"
    scout_id = Column(Integer, ForeignKey("scout.id"), primary_key=True)
    scout = relationship("Scout", back_populates="mb_needs")
    requirement_badge = Column(String, primary_key=True)
    requirement_code = Column(String, primary_key=True)
    requirement = relationship("MBRequirement", back_populates="needs")
    need = Column(Boolean)
    __table_args__ = (
        ForeignKeyConstraint(("requirement_badge", "requirement_code"),
                             ("mb_requirement.badge", "mb_requirement.code")),
    )

    def __repr__(self):
        return f"<MB Requirement Need: {self.scout}, {self.requirement}, {self.need}"

"""
requirement.py

models a BSA rank requirement

"""
from sqlalchemy import (
    Column,
    String,
    ForeignKeyConstraint,
)
from sqlalchemy.orm import relationship
from flask_troop.models.base import Base, session


class Requirement(Base):
    __tablename__ = "requirement"
    rank_name = Column(String, primary_key=True)
    rank = relationship("Rank", back_populates="requirements")
    code = Column(String, primary_key=True)
    text = Column(String)
    groups = relationship("RGroup", secondary="requirement_rgroup", back_populates="requirements")
    signoffs = relationship("Signoff", back_populates="requirement")
    __table_args__ = (
        ForeignKeyConstraint(('rank_name',), ('rank.name',)),
    )
    def __repr__(self):
        return f"<{self.rank.name}-{self.code}: {self.text}>"

    @classmethod
    def get(cls, rank, code):
        return session.query(Requirement)\
            .filter(Requirement.rank_name == rank.name)\
            .filter(Requirement.code == code)\
            .one_or_none()

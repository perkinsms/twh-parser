from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    ForeignKeyConstraint,
)
from sqlalchemy.orm import relationship
from flask_troop.models.base import Base


class RGroup(Base):
    __tablename__ = "rgroup"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    requirements = relationship(
        "Requirement", secondary="requirement_rgroup", back_populates="groups"
    )


class RequirementRGroup(Base):
    __tablename__ = "requirement_rgroup"
    group_id = Column(Integer, ForeignKey("rgroup.id"), primary_key=True)
    requirement_rank = Column(String, primary_key=True)
    requirement_code = Column(Integer, primary_key=True)
    __table_args__ = (
        ForeignKeyConstraint(('requirement_rank','requirement_code'),
                             ('requirement.rank_name','requirement.code')),
    )

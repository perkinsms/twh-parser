"""
group.py

sqlalchemy model for a group of scouts (e.g. a patrol)

Group
columns:
name PK
scouts relationship (scout) -> many to many using scout_group as secondary table

ScoutGroup
columns:
scout_id PK FK (scout.id)
group_id PK FK (group.name)

"""
from sqlalchemy import (
    Column,
    Integer,
    String,
    ForeignKey,
    )
from sqlalchemy.orm import relationship 
from sqlalchemy.exc import MultipleResultsFound 
from flask_troop.models.base import Base, session


class Group(Base):
    __tablename__ = "group"
    name = Column(String, primary_key=True)
    scouts = relationship("Scout", secondary="scout_group", back_populates="groups")

    def __repr__(self):
        return f"<Group: {self.name}>"

    @classmethod
    def get_group(cls, name):
        try:
            return session.query(Group).filter(Group.name == name).one_or_none()
        except MultipleResultsFound:
            print(session.query(Group).filter(Group.name == name).all())
            print(f"Multiple Results Found: {name}")
            return None
        

class ScoutGroup(Base):
    __tablename__ = "scout_group"
    scout_id = Column(Integer, ForeignKey('scout.id'), primary_key=True)
    group_id = Column(String, ForeignKey('group.name'), primary_key=True)

"""
scout.py

"""
from sqlalchemy import (
    Column,
    String,
    ForeignKeyConstraint,
    Integer,
    )
from sqlalchemy.orm import relationship
from sqlalchemy.exc import MultipleResultsFound
from flask_troop.models.base import Base, session


class Scout(Base):
    __tablename__ = "scout"
    id = Column(Integer, primary_key=True)
    name = Column(String)
    age = Column(String)
    patrol = Column(String)
    current_rank_name = Column(String)
    current_rank = relationship("Rank")
    signoffs = relationship("Signoff")
    mb_needs = relationship("MBReqNeed", back_populates="scout")
    groups = relationship("Group", secondary="scout_group", back_populates="scouts")
    __table_args__ = (
        ForeignKeyConstraint(('current_rank_name',), ('rank.name',)),
        )

    def __repr__(self):
        return f"<Scout {self.name} ({self.current_rank})>"

    @classmethod
    def get_scout(cls, name):
        try:
            return session.query(Scout).filter(Scout.name == name).one_or_none()
        except MultipleResultsFound:
            print("Multiple Scouts Found")

    def signoff_needs(self, requirements=None):
        if requirements is None:
            return [s.requirement for s in self.signoffs if s.date is None]
        else:
            return [s.requirement for s in self.signoffs if s.date is None and s.requirement in requirements]
            
    @classmethod
    @property
    def all(cls):
        return session.query(Scout).all()
"""
rank.py

models a BSA rank

"""
from sqlalchemy import (
    Column,
    String,
    Integer,
    Boolean,
)
from sqlalchemy.orm import relationship
from flask_troop.models.base import Base, session


class Rank(Base):
    __tablename__ = "rank"
    name = Column(String, primary_key=True)
    upper_rank = Column(Boolean)
    extra_rank = Column(Boolean)
    rank_order = Column(Integer)
    final_code = Column(String)
    requirements = relationship("Requirement", back_populates="rank")

    def __repr__(self):
        return f"<rank {self.name}>"

    def __index__(self):
        return self.rank_order

    @classmethod
    def get_rank(cls, rank_name):
        return session.query(Rank).filter(Rank.name == rank_name).one_or_none()

    @classmethod
    def get_requirements(cls, rank_name):
        return cls.get_rank(rank_name).requirements

    @classmethod
    @property
    def all(cls):
        return session.query(Rank).all()
from sqlalchemy import (
    create_engine,
    MetaData,
)

from sqlalchemy.orm import (
    declarative_base,
    sessionmaker,
)

filename = "data/scouts.db"

Base = declarative_base()
engine = create_engine(f"sqlite:///{filename}", echo=False, future=True)

session = sessionmaker(bind=engine)()

metadata = MetaData(bind=engine)

"""
signoff.py

sqlalchemy model for a signoff
date
scout_id -> PK, FK (scout.id)
scout -> relationship (scout)
requirement_rank -> PK, FK (requirement.rank_name)
requirement_code -> PK, FK (requirement.code)

"""
from sqlalchemy import (
    Column,
    Date,
    Integer,
    String,
    ForeignKey,
    ForeignKeyConstraint,
)
from sqlalchemy.orm import relationship
from flask_troop.models.base import Base


class Signoff(Base):
    __tablename__ = "signoff"
    date = Column(Date)
    scout_id = Column(Integer, ForeignKey("scout.id"), primary_key=True)
    scout = relationship("Scout", back_populates="signoffs")
    rank = Column(String, primary_key=True)
    code = Column(String, primary_key=True)
    requirement = relationship("Requirement", back_populates="signoffs")
    __table_args__ = (
        ForeignKeyConstraint(("rank", "code"),
                             ("requirement.rank_name", 'requirement.code')),
    )

    def __repr__(self):
        if self.date is not None:
            date = self.date.strftime("%m/%d/%Y")
        else:
            date = "N/A"
        return (
            f"<signoff of {self.requirement.rank.name}-{self.requirement.code} for {self.scout.name} on {date}>"
        )

from datetime import datetime
from flask import Flask

from config import Config


def create_app(config_class=Config):
    app = Flask(__name__)
    app.config.from_object(config_class)

    # Flask Extensions Here

    # Register blueprints here
    from flask_troop.main import bp as main_bp
    app.register_blueprint(main_bp)

    @app.route("/test/")
    def test_page():
        return "<h1>Testing the Flask Application Factory Pattern</h1>"

    return app


def parse_scout(text):
    return ', '.join((text.split(', ')[0], text.split(', ')[1].split()[0]))


def parse_code(text):
    return text.strip().lstrip('0').replace('.', "")


def get_date_earned(text):
    return datetime.strptime(text, "%m/%d/%y").date() if text else None



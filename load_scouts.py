"""
load_scouts.py

loads scouts from text file

depends:
load_ranks
load_groups

usage:
import load_scouts
load_scouts.main()
"""
import csv
from flask_troop.models import (session,
                                Scout,
                                Group,
                                Rank
                                )

from flask_troop import (parse_scout,
                               )
requirements_file = "data/rank_requirements.csv"


def add_scout(name):
    if (
        s := session.query(Scout)
        .filter(Scout.name == parse_scout(name))
        .one_or_none()
    ):
        pass
    else:
        s = Scout(name=parse_scout(name))
        session.add(s)
    return s


def add_more_information(scout, line):
    scout.patrol = line["Patrol"]
    scout.age = line["Age"]
    scout.current_rank = Rank.get_rank(line["Current Rank"])


def add_scout_to_group(scout, group):
    scout.groups.append(group)


def import_scouts(file):
    with open(file, encoding="utf-8-sig") as f:
        reader = csv.DictReader(f)
        for line in reader:
            group = Group.get_group(line['Patrol'])
            if not Scout.get_scout(parse_scout(line['Scout'])):
                scout = add_scout(line['Scout'])
                add_more_information(scout, line)
                add_scout_to_group(scout, group)
        session.commit()


def main():
    import_scouts(requirements_file)


if __name__ == "__main__":
    main()

"""load_requirements.py

used to load the requirements from file

depends on load_ranks

usage:
import load_requirements
load_requirements.main()

"""
import csv
from flask_troop.models import (session,
                                engine,
                                Base,
                                Requirement,
                                Rank,
                                )
from flask_troop import (parse_code,
                               )

requirements_file = "data/rank_requirements.csv"


def add_requirement(line):
    r = Requirement(
        rank=Rank.get_rank(line['Rank']), code=parse_code(line["Code"]), text=line["Requirement"]
    )
    session.add(r)


def import_requirements(file):
    with open(file, encoding="utf-8-sig") as f:
        reader = csv.DictReader(f)
        for line in reader:
            rank = Rank.get_rank(line['Rank'])
            code = parse_code(line["Code"])
            if not Requirement.get(rank=rank, code=code):
                add_requirement(line)
        session.commit()


def main():
    import_requirements(requirements_file)


if __name__ == "__main__":
    main()
